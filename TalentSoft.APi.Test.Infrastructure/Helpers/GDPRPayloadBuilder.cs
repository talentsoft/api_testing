﻿namespace TalentSoft.APi.Test.Infrastructure.Helpers
{
    using Newtonsoft.Json;
    using TalentSoft.APi.Test.Infrastructure.Converters;

    public partial class GDPRPayloadBuilder
    {
        [JsonProperty("email")]
        public string Email { get; set; }

        [JsonProperty("employeeNumber")]
        public string EmployeeNumber { get; set; }

        [JsonProperty("firstName")]
        public string FirstName { get; set; }

        [JsonProperty("lastName")]
        public string LastName { get; set; }
    }
    public partial class GDPRPayloadBuilder
    {
        public static GDPRPayloadBuilder FromJson(string json) => JsonConvert.DeserializeObject<GDPRPayloadBuilder>(json, JsonConverterSettings.Settings);
    }

    public static class Serialize
    {
        public static string ToJson(this GDPRPayloadBuilder self) => JsonConvert.SerializeObject(self, JsonConverterSettings.Settings);
        public static string ToJsonIgnoreNull(this GDPRPayloadBuilder self) => JsonConvert.SerializeObject(self, JsonConverterSettings.NullSettings);
    }
}
