﻿namespace TalentSoft.APi.Test.Infrastructure.Helpers
{
    using System.Configuration;
    public static class ApiEndpointHelper
    {
        public static string SetApiEndpoint(string apiDomain)
        {
            string apiEndpoint = ConfigurationManager.AppSettings[$"{apiDomain.ToLowerInvariant()}"];
            return apiEndpoint;
        }
    }
}
