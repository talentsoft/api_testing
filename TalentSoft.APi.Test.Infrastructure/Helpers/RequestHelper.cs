﻿namespace TalentSoft.APi.Test.Infrastructure.Helpers
{
    using RestSharp;

    public static class RequestHelper
    {
        public static IRestRequest CreateRequestWithPayloadMethodTimeout(string route, Method method, object body, int timeout)
        {
            IRestRequest request = new RestRequest(route, method);

            if (timeout != 0) request.Timeout = timeout;

            if (body != null) request.AddJsonBody(body);

            return request;
        }
        public static IRestRequest CreateRequestWithPayloadAndMethod(string route, Method method, object body)
        {
            IRestRequest request = new RestRequest(route, method);

            if (body != null) request.AddJsonBody(body);

            return request;
        }
        public static IRestRequest CreateRequestWithTimeoutAndMethod(string route, Method method, int timeout)
        {
            IRestRequest request = new RestRequest(route, method);

            if (timeout != 0) request.Timeout = timeout;

            return request;
        }
        public static IRestRequest CreateRequest(string route)
        {
            IRestRequest request = new RestRequest(route);
            return request;
        }
        public static IRestRequest CreateRequestWithMethod(string route, Method method)
        {
            IRestRequest request = new RestRequest(route, method);
            return request;
        }

    }
}
