﻿namespace TalentSoft.APi.Test.Infrastructure.Helpers
{
    using System.Configuration;
    public static class SqlConnectionHelper
    {
        public static string SetConnectionString(string dbName)
        {
            string connectionString = ConfigurationManager.ConnectionStrings[$"{dbName.ToLowerInvariant()}"].ConnectionString;
            return connectionString;
        }
    }
}
