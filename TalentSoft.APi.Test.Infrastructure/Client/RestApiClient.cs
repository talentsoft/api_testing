﻿namespace TalentSoft.APi.Test.Infrastructure.Client
{
    using RestSharp;
    using TalentSoft.APi.Test.Infrastructure.Auth;
    using TalentSoft.APi.Test.Infrastructure.Managers;
    public class RestApiClient
    {
        public RestClient CreateTokenClient(string Module, string token)
        {
           var client = new RestClient(ApiManager.ApiEndpointBase($"{Module}"));
            {
                client.AddDefaultHeader("Authorization",
                    string.Format("Bearer {0}",token));
                client.AddDefaultHeader("Content-Type", "application/json");
                client.AddDefaultHeader("Accept", "application/json");
            }
            return client;
        }
        public RestClient CreateTokenClient(string User, string Password, string Module, string token)
        {
            var client = new RestClient(ApiManager.ApiEndpointBase($"{Module}"));
            {
                client.AddDefaultHeader("Authorization",
                    string.Format("Bearer {0}",token));
                client.AddDefaultHeader("Content-Type", "application/json");
                client.AddDefaultHeader("Accept", "application/json");

            }
            return client;
        }
        public RestClient CreateApp2AppTokenClient(string Module)
        {
            var client = new RestClient(ApiManager.ApiEndpointBase($"{Module}"));
            {
                client.AddDefaultHeader("Authorization",
                    string.Format("Bearer {0}",
                     new App2AppAuthenticationToken().AutheticationToken()));
                client.AddDefaultHeader("Content-Type", "application/json");
                client.AddDefaultHeader("Accept", "application/json");

            }
            return client;
        }
        public RestClient CreateCookieClient(string Module)
        {
            var client = new RestClient(ApiManager.ApiEndpointBase($"{Module}"));
            {
                client.CookieContainer = AuthenticationCookie.SetCookieValue(ApiManager.ApiEndpointBase($"{Module}"));
                client.AddDefaultHeader("Accept", "application/json");
                client.AddDefaultHeader("Content-Type", "application/json");

            }
            return client;
        }
        public RestClient CreateCookieClient(string User, string Password, string Module)
        {
             var client = new RestClient(ApiManager.ApiEndpointBase($"{Module}"));
            {
                client.CookieContainer = AuthenticationCookie.SetCookieValue(ApiManager.ApiEndpointBase($"{Module}"), User, Password);
                client.AddDefaultHeader("Accept", "application/json");
                client.AddDefaultHeader("Content-Type", "application/json");
            }
            return client;
        }
    }
}
