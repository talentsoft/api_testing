﻿namespace TalentSoft.APi.Test.Infrastructure.Managers
{
    using System.Configuration;
    public static class AuthManager
    {
        public static string AdminUserName => ConfigurationManager.AppSettings["adminUserName"];
        public static string AdminPassword => ConfigurationManager.AppSettings["adminPassword"];
        public static string User(string user) => ConfigurationManager.AppSettings[$"{user}"];
        public static string Password(string password) => ConfigurationManager.AppSettings[$"{password}"];
    }
}
