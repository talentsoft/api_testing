﻿namespace TalentSoft.APi.Test.Infrastructure.Managers
{
    using TalentSoft.APi.Test.Infrastructure.Helpers;

    public static class ApiManager
    {
        public static string ApiEndpointBase(string module)
        {
            return ApiEndpointHelper.SetApiEndpoint(module);
        }

    }
}
