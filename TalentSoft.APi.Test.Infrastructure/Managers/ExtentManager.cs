﻿namespace TalentSoft.APi.Test.Infrastructure.Managers
{
    using System;
    using NUnit.Framework;
    using System.Configuration;
    using AventStack.ExtentReports;
    using AventStack.ExtentReports.Reporter;
    using AventStack.ExtentReports.Reporter.Configuration;

    public static class ExtentManager
    {
        private static readonly Lazy<ExtentReports> Lazy = new Lazy<ExtentReports>(() => new ExtentReports());

        public static ExtentReports Instance => Lazy.Value;

        static ExtentManager()
        {
            var env = ConfigurationManager.AppSettings["envVariable"];
            var htmlReporter =
                new ExtentHtmlReporter(
                    TestContext.CurrentContext.TestDirectory.Replace($"\\bin\\{env}\\net472", "\\TestResults") +
                    "\\Extent.html");
            htmlReporter.Config.DocumentTitle = "Tests Run Statuses";
            htmlReporter.Config.ReportName = "Test Run Status";
            htmlReporter.Config.EnableTimeline = true;
            htmlReporter.Config.Theme = Theme.Dark;

            Instance.AttachReporter(htmlReporter);
        }
    }
}
