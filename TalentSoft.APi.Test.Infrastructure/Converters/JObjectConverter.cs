﻿namespace TalentSoft.APi.Test.Infrastructure.Converters
{
    using System;
    using Newtonsoft.Json.Linq;
    using System.Collections.Generic;
    public class JObjectConverter
    {
        public static List<string> InvalidJsonElements;
        public static IList<T> DeserializeToList<T>(string jsonString)
        {
            InvalidJsonElements = null;
            var array = JArray.Parse(jsonString);
            IList<T> objectsList = new List<T>();

            foreach (var item in array)
            {
             objectsList.Add(item.ToObject<T>());
            }

            return objectsList;
        }
    }
}
