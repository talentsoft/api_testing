﻿namespace TalentSoft.APi.Test.Infrastructure.TestInitializer
{
    using NUnit.Framework;
    using System.Diagnostics;
    using TalentSoft.APi.Test.Infrastructure.Loggers;
    using TalentSoft.APi.Test.Infrastructure.Managers;

    public class TestInitializer
    {
        private Stopwatch _stopwatch;

        [OneTimeSetUp]
        protected void Initialize()
        {  
            ExtentTestManager.CreateParentTest(GetType().Name);
        }

        [OneTimeTearDown]
        protected void TearDown()
        { 
            ExtentManager.Instance.Flush();
        }

        [SetUp]
        protected void BeforeTest()
        {
            _stopwatch = Stopwatch.StartNew();
            ExtentTestManager.CreateTest(TestContext.CurrentContext.Test.Name);
        }

        [TearDown]
        protected void AfterTest()
        {
            _stopwatch.Stop();
            ExtentLogger.Log(_stopwatch);
        }
    }
}
