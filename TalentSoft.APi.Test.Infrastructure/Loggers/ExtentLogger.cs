﻿namespace TalentSoft.APi.Test.Infrastructure.Loggers
{
    using AventStack.ExtentReports;
    using NUnit.Framework;
    using NUnit.Framework.Interfaces;
    using TalentSoft.APi.Test.Infrastructure.Managers;
    using System.Diagnostics;

    public static class ExtentLogger
    {
        public static void Log(Stopwatch timeElapsed)
        {
            var status = TestContext.CurrentContext.Result.Outcome.Status;
            var stacktrace = string.IsNullOrEmpty(TestContext.CurrentContext.Result.StackTrace)
                    ? ""
                    : $"<pre>{TestContext.CurrentContext.Result.StackTrace}</pre>";
            var expectedOutcome = string.IsNullOrEmpty(TestContext.CurrentContext.Result.Message)
                    ? ""
                    : $"<pre>{TestContext.CurrentContext.Result.Message}</pre>";
            var testDuration = $"<pre>{"Test Duration:" + timeElapsed.Elapsed}</pre>";
            Status logStatus;

            switch (status)
            {
                case TestStatus.Failed:
                    logStatus = Status.Fail;
                    break;
                case TestStatus.Inconclusive:
                    logStatus = Status.Warning;
                    break;
                case TestStatus.Skipped:
                    logStatus = Status.Skip;
                    break;
                default:
                    logStatus = Status.Pass;
                    break;
            }

            ExtentTestManager.GetTest().Log(logStatus, "Test ended with " + logStatus + stacktrace + expectedOutcome + testDuration);
        }
    }
}
