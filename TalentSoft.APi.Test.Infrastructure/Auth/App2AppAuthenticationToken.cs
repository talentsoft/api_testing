﻿namespace TalentSoft.APi.Test.Infrastructure.Auth
{
    using RestSharp;
    using System.Linq;
    using System.Configuration;
    using Newtonsoft.Json.Linq;

    public class App2AppAuthenticationToken
    {
        public static string App2AppUri => ConfigurationManager.AppSettings["app2appuri"];
        public string AutheticationToken()
        {
            var client = new RestClient(App2AppUri);
            client.Timeout = -1;
            var request = new RestRequest("connect/token",Method.POST);
            request.AddHeader("Accept", "application/json");
            request.AddHeader("Content-Type", "application/x-www-form-urlencoded");
            request.AddParameter("grant_type", "client_credentials");
            request.AddParameter("client_id", "talentsoft_privacypolicy");
            request.AddParameter("client_secret", "talentsoft_privacypolicy");
            request.AddParameter("scope", "Instance.gdpr-master");
            var response = client.Execute(request);
            var token = response.Content;
            dynamic jsonData = JObject.Parse(token);
            var tokenValue = jsonData.access_token;
            return tokenValue;
        }
    }
}
