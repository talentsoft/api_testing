﻿namespace TalentSoft.APi.Test.Infrastructure.Auth
{
    using System;
    using System.Web;
    using System.Linq;
    using System.Collections.Generic;
    using TalentSoft.APi.Test.Infrastructure.Auth.AuthPage;

    public class GDPRAuthenticationToken 
    {
        public string GetJwtToken(string baseUrl, string username , string password)
        {
            LoginFlow flow = new LoginFlow();
            flow.Login(baseUrl, username, password);
            string uri = flow.finalRedirectUri;
            var queryStringParameters = GetQueryStringParameters(uri);
            string tokenValue = queryStringParameters["access_token"];
            return tokenValue;
        }

        private Dictionary<string,string> GetQueryStringParameters(string uri)
        {
            var decodedUri = HttpUtility.UrlDecode(uri);
            Uri newUri = new Uri(decodedUri);
            var segments = newUri.Query.ToString().Replace("?", "");

            var queryStringParameters = segments.Split('&')
                                    .Select(parameter => parameter.Split('='))
                                    .ToDictionary(parameter => parameter[0], parameter => parameter.Length > 1 ? Uri.UnescapeDataString(parameter[1]) : null);
            return queryStringParameters;

        }
    }
}
