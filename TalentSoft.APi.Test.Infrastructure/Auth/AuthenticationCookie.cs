﻿namespace TalentSoft.APi.Test.Infrastructure.Auth
{
    using System.Net;
    using RestSharp;
    using TalentSoft.APi.Test.Infrastructure.Managers;

    public static class AuthenticationCookie
    {
        public static CookieContainer SetCookieValue(string baseUrl)
        {
            CookieContainer cookieJar = new CookieContainer();
            var client = new RestClient(baseUrl)
            {
                CookieContainer = cookieJar
            };
            var request = new RestRequest("api/authentication", Method.POST);
            request.AddParameter("login", $"{AuthManager.AdminUserName}");
            request.AddParameter("password", $"{AuthManager.AdminPassword}");
            _ = client.Execute(request);

            return cookieJar;
        }

        public static CookieContainer SetCookieValue(string baseUrl, string user, string password)
        {
            CookieContainer cookieJar = new CookieContainer();
            var client = new RestClient(baseUrl)
            {
                CookieContainer = cookieJar
            };
            var request = new RestRequest("api/authentication", Method.POST);
            request.AddParameter("login", $"{AuthManager.User(user)}");
            request.AddParameter("password", $"{AuthManager.Password(password)}");
            _ = client.Execute(request);

            return cookieJar;

        }
    }
}

