﻿namespace TalentSoft.APi.Test.Infrastructure.Auth.AuthPage.Interface
{
    using OpenQA.Selenium;
    public interface IIdentityProviderPage
    {
        void LogIn(IWebDriver driver, string user, string password);
        string GetRefererUri();
    }
}
