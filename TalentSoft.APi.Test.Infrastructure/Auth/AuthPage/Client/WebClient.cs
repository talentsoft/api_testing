﻿namespace TalentSoft.APi.Test.Infrastructure.Auth.AuthPage.Client
{
    using OpenQA.Selenium;
    using OpenQA.Selenium.Chrome;

    public class WebClient
    {
        public IWebDriver _driver;

        public void Initialize()
        {
            var options = new ChromeOptions();
            options.AddArguments("window-size=800,600");
            var driver = new ChromeDriver(options);
            _driver = driver;
        }
    }
}
