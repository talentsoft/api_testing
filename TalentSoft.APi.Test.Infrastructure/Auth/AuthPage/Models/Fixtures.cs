﻿namespace TalentSoft.APi.Test.Infrastructure.Auth.AuthPage.Models
{
    using TalentSoft.APi.Test.Infrastructure.Auth.AuthPage.Interface;
    using TalentSoft.APi.Test.Infrastructure.Auth.AuthPage.Models.IdentityProvider;
    public static class Fixtures
    {

        private static IdentityProviderPage _identityProviderPage;

        public static IIdentityProviderPage identityProvider =>
            _identityProviderPage ?? (_identityProviderPage = new IdentityProviderPage());
    }
}
