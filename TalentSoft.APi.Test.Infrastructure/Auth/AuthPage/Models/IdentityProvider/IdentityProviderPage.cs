﻿namespace TalentSoft.APi.Test.Infrastructure.Auth.AuthPage.Models.IdentityProvider
{
    using System;
    using OpenQA.Selenium;
    using OpenQA.Selenium.Support.UI;
    using TalentSoft.APi.Test.Infrastructure.Managers;
    using TalentSoft.APi.Test.Infrastructure.Auth.AuthPage.Interface;

    public class IdentityProviderPage : IIdentityProviderPage
    {
        private IWebDriver _driver;
        public IWebElement userNameField => _driver.FindElement(By.Id("Username"));
        public IWebElement passwordField => _driver.FindElement(By.Id("Password"));
        public IWebElement signIn => _driver.FindElement(By.CssSelector("button.btn.btn-primary.btn-lg"));

        public string reffererUri;

        public void LogIn(IWebDriver driver, string user , string password)
        {
            var uservalue = AuthManager.User($"{user}");
            var passwordValue = AuthManager.Password($"{password}");
            _driver = driver;
            userNameField.SendKeys($"{uservalue}");
            passwordField.SendKeys($"{passwordValue}");
            signIn.Click();
            WebDriverWait wait = new WebDriverWait(_driver, TimeSpan.FromSeconds(30));
            wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.UrlContains("access_token"));
            reffererUri = _driver.Url;
            _driver.Close();
            _driver.Dispose();
        }

        public string GetRefererUri()
        {
            return reffererUri;
        }
    }
}
