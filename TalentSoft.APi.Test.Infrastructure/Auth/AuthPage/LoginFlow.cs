﻿namespace TalentSoft.APi.Test.Infrastructure.Auth.AuthPage
{
    using TalentSoft.APi.Test.Infrastructure.Auth.AuthPage.Client;
    using TalentSoft.APi.Test.Infrastructure.Auth.AuthPage.Models;
    public class LoginFlow : WebClient
    {
        public string finalRedirectUri;

        public void Login(string baseDomain, string username , string password)
        {
            Initialize();
            _driver.Navigate().GoToUrl(baseDomain);
            Fixtures.identityProvider.LogIn(_driver,username,password);
            finalRedirectUri= Fixtures.identityProvider.GetRefererUri();
        }
    }
}
