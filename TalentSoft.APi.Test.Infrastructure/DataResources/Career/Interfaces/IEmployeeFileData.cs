﻿namespace TalentSoft.APi.Test.Infrastructure.DataResources.Career.Interfaces
{
    using RestSharp;
    using System.Collections.Generic;
    using TalentSoft.APi.Test.Infrastructure.Models.Career.api.v1.directory.employeefile;
    public interface IEmployeeFileData
    {
        EmployeeFile EmployeeFileColleagueList(RestClient client, int pageIndex, int pageSize);
        EmployeeFile EmployeeFileManageesList(RestClient client, int pageIndex, int pageSize);
        EmployeeFile EmployeeFileIndividualSearchList(RestClient client, string searchTerm, int pageIndex, int pageSize);
        EmployeeFile EmployeeFileIndividualSearchList(RestClient client, string searchTerm, int pageIndex, int pageSize, bool includeInactives, bool onlyEmployees);

    }
}
