﻿namespace TalentSoft.APi.Test.Infrastructure.DataResources.Career.Interfaces
{
    using RestSharp;
    using System.Collections.Generic;
    using TalentSoft.APi.Test.Infrastructure.Models.Career.api.v1.directory.individuals;
    public interface IIndividualsData
    {
        List<Result> IndividualList(RestClient client, int count, int offset);

    }
}
