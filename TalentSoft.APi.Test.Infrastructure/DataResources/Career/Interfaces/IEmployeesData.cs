﻿namespace TalentSoft.APi.Test.Infrastructure.DataResources.Career.Interfaces
{
    using RestSharp;
    using System.Collections.Generic;
    using TalentSoft.APi.Test.Infrastructure.Models.Career.api.v1.directory.employees;

    public interface IEmployeesData
    {
        List<Result> EmployeeList(RestClient client, int count, int offset);
    }
}
