﻿namespace TalentSoft.APi.Test.Infrastructure.DataResources.Career.Resources
{
    using RestSharp;
    using System.Collections.Generic;
    using TalentSoft.APi.Test.Infrastructure.DataResources.Career.Interfaces;
    using TalentSoft.APi.Test.Infrastructure.Models.Career.api.v1.directory.individuals;

    public class IndividualsData : IIndividualsData
    {
        /// <summary>
        /// Gets a list of individuals.
        /// </summary>
        /// <param name="client">RestClient used in the tests.</param>
        /// <param name="count">Number of employees returned.</param>
        /// <param name="offset">Number of skyped employees.</param>
        public List<Result> IndividualList(RestClient client, int count, int offset)
        {

            var request = new RestRequest("/api/v1/directory/individuals");
            request.AddParameter("count", $"{count}");
            request.AddParameter("offset", $"{offset}");
            var response = client.Execute(request);
            var responseContent = Individuals.FromJsonIgnoreNull(response.Content);
            var resultList = responseContent.Results;

            return resultList;
        }
    }
}
