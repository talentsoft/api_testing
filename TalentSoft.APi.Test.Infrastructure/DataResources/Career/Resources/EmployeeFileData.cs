﻿namespace TalentSoft.APi.Test.Infrastructure.DataResources.Career.Resources
{
    using RestSharp;
    using System.Collections.Generic;
    using TalentSoft.APi.Test.Infrastructure.DataResources.Career.Interfaces;
    using TalentSoft.APi.Test.Infrastructure.Models.Career.api.v1.directory.employeefile;
    public class EmployeeFileData : IEmployeeFileData
    {
        public EmployeeFile EmployeeFileColleagueList(RestClient client, int pageIndex, int pageSize)
        {

            var request = new RestRequest("api/v1/directory/my/organization/colleagues");
            request.AddParameter("pageIndex", $"{pageIndex}");
            request.AddParameter("pageSize", $"{pageSize}");
            var response = client.Execute(request);
            var responseContent = EmployeeFile.FromJsonIgnoreNull(response.Content);

            return responseContent;
        }
        public EmployeeFile EmployeeFileManageesList(RestClient client, int pageIndex, int pageSize)
        {

            var request = new RestRequest("api/v1/directory/my/organization/managees");
            request.AddParameter("pageIndex", $"{pageIndex}");
            request.AddParameter("pageSize", $"{pageSize}");
            var response = client.Execute(request);
            var responseContent = EmployeeFile.FromJsonIgnoreNull(response.Content);

            return responseContent;

        }
        public EmployeeFile EmployeeFileIndividualSearchList(RestClient client, string searchTerm, int pageIndex, int pageSize, bool includeInactives, bool onlyEmployees)
        {

            var request = new RestRequest("api/v1/directory/individualSearch");
            request.AddParameter("searchTerm", $"{searchTerm}");
            request.AddParameter("pageIndex", $"{pageIndex}");
            request.AddParameter("pageSize", $"{pageSize}");
            request.AddParameter("includeInactives", $"{includeInactives}");
            request.AddParameter("onlyEmployees", $"{onlyEmployees}");
            var response = client.Execute(request);
            var responseContent = EmployeeFile.FromJsonIgnoreNull(response.Content);

            return responseContent;
        }
        public EmployeeFile EmployeeFileIndividualSearchList(RestClient client, string searchTerm, int pageIndex, int pageSize)
        {

            var request = new RestRequest("api/v1/directory/individualSearch");
            request.AddParameter("searchTerm", $"{searchTerm}");
            request.AddParameter("pageIndex", $"{pageIndex}");
            request.AddParameter("pageSize", $"{pageSize}");
            var response = client.Execute(request);
            var responseContent = EmployeeFile.FromJsonIgnoreNull(response.Content);

            return responseContent;
        }
    }
}
