﻿namespace TalentSoft.APi.Test.Infrastructure.DataResources.Career
{
    using TalentSoft.APi.Test.Infrastructure.DataResources.Career.Resources;
    using TalentSoft.APi.Test.Infrastructure.DataResources.Career.Interfaces;

    public class CareerDataResources
    {
        private static EmployeesData _employeesData;
        public static IEmployeesData EmployeesData =>
            _employeesData ?? (_employeesData = new EmployeesData());

        private static IndividualsData _individualsData;
        public static IIndividualsData IndividualsData =>
            _individualsData ?? (_individualsData = new IndividualsData());

        private static EmployeeFileData _employeeFileData;
        public static IEmployeeFileData EmployeeFileData =>
            _employeeFileData ?? (_employeeFileData = new EmployeeFileData());
    }
}
