﻿namespace TalentSoft.APi.Test.Infrastructure.Models.Career.api.v1.directory.employees
{
    using System;
    using System.Collections.Generic;
    using Newtonsoft.Json;
    using TalentSoft.APi.Test.Infrastructure.Converters;


    public partial class Employees
    {
        [JsonProperty("totalCount")]
        public long TotalCount { get; set; }

        [JsonProperty("results")]
        public List<Result> Results { get; set; }
    }

    public partial class Result
    {
        [JsonProperty("employeeNumber")]
        public long EmployeeNumber { get; set; }

        [JsonProperty("individual")]
        public Individual Individual { get; set; }

        [JsonProperty("referenceAdmissionDate")]
        public DateTimeOffset ReferenceAdmissionDate { get; set; }

        [JsonProperty("referenceEndDate")]
        public object ReferenceEndDate { get; set; }

        [JsonProperty("groupAdmissionDate")]
        public DateTimeOffset GroupAdmissionDate { get; set; }

        [JsonProperty("candidateId")]
        public object CandidateId { get; set; }

        [JsonProperty("onboarding")]
        public bool Onboarding { get; set; }
    }

    public partial class Individual
    {
        [JsonProperty("individualId")]
        public long IndividualId { get; set; }

        [JsonProperty("firstName")]
        public string FirstName { get; set; }

        [JsonProperty("lastName")]
        public string LastName { get; set; }

        [JsonProperty("user")]
        public User User { get; set; }

        [JsonProperty("sex")]
        public long? Sex { get; set; }

        [JsonProperty("civilStatus")]
        public CivilStatus CivilStatus { get; set; }

        [JsonProperty("birthdate")]
        public DateTimeOffset? Birthdate { get; set; }

        [JsonProperty("isDefaultImage")]
        public bool IsDefaultImage { get; set; }

        [JsonProperty("pictureUrl")]
        public string PictureUrl { get; set; }

        [JsonProperty("modificationDate")]
        public DateTimeOffset ModificationDate { get; set; }

        [JsonProperty("creationDate")]
        public DateTimeOffset CreationDate { get; set; }
    }

    public partial class CivilStatus
    {
        [JsonProperty("nationality")]
        public string Nationality { get; set; }

        [JsonProperty("title")]
        public string Title { get; set; }

        [JsonProperty("birthName")]
        public string BirthName { get; set; }

        [JsonProperty("otherFirstName")]
        public string OtherFirstName { get; set; }
    }

    public partial class User
    {
        [JsonProperty("username")]
        public string Username { get; set; }

        [JsonProperty("email")]
        public string Email { get; set; }

        [JsonProperty("activeAccount")]
        public bool ActiveAccount { get; set; }

        [JsonProperty("password")]
        public object Password { get; set; }
    }

    public partial class Employees 
    {
        public static Employees FromJson(string json) => JsonConvert.DeserializeObject<Employees>(json, JsonConverterSettings.Settings);
        public static Employees FromJsonIgnoreNull(string json) => JsonConvert.DeserializeObject<Employees>(json, JsonConverterSettings.NullSettings);
    }

    public static class Serialize
    {
        public static string ToJson(this Employees self) => JsonConvert.SerializeObject(self, JsonConverterSettings.Settings);
        public static string ToJsonIgnoreNull(this Employees self) => JsonConvert.SerializeObject(self, JsonConverterSettings.NullSettings);
    }

}
