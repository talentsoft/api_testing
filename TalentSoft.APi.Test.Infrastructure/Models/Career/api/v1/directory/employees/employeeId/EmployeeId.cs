﻿//    To access data from Response use the following in the test files.
//    using TalentSoft.APi.Test.Infrastructure.Models.Career.api.v1.employees.EmployeeId;
//    var employeeId = EmployeeId.FromJson(jsonString);

namespace TalentSoft.APi.Test.Infrastructure.Models.Career.api.v1.directory.employees.employeeId
{
    using System;
    using Newtonsoft.Json;
    using TalentSoft.APi.Test.Infrastructure.Converters;

    public partial class EmployeeId
    {
        [JsonProperty("employeeNumber")]
        public string EmployeeNumber { get; set; }

        [JsonProperty("individual")]
        public Individual Individual { get; set; }

        [JsonProperty("referenceAdmissionDate")]
        public DateTimeOffset ReferenceAdmissionDate { get; set; }

        [JsonProperty("referenceEndDate")]
        public object ReferenceEndDate { get; set; }

        [JsonProperty("groupAdmissionDate")]
        public DateTimeOffset GroupAdmissionDate { get; set; }

        [JsonProperty("candidateId")]
        public object CandidateId { get; set; }

        [JsonProperty("onboarding")]
        public bool Onboarding { get; set; }
    }

    public partial class Individual
    {
        [JsonProperty("individualId")]
        public long IndividualId { get; set; }

        [JsonProperty("firstName")]
        public string FirstName { get; set; }

        [JsonProperty("lastName")]
        public string LastName { get; set; }

        [JsonProperty("user")]
        public User User { get; set; }

        [JsonProperty("sex")]
        public long Sex { get; set; }

        [JsonProperty("civilStatus")]
        public CivilStatus CivilStatus { get; set; }

        [JsonProperty("birthdate")]
        public DateTimeOffset Birthdate { get; set; }

        [JsonProperty("isDefaultImage")]
        public bool IsDefaultImage { get; set; }

        [JsonProperty("pictureUrl")]
        public string PictureUrl { get; set; }

        [JsonProperty("modificationDate")]
        public DateTimeOffset ModificationDate { get; set; }

        [JsonProperty("creationDate")]
        public DateTimeOffset CreationDate { get; set; }
    }

    public partial class CivilStatus
    {
        [JsonProperty("nationality")]
        public object Nationality { get; set; }

        [JsonProperty("title")]
        public object Title { get; set; }

        [JsonProperty("birthName")]
        public object BirthName { get; set; }

        [JsonProperty("otherFirstName")]
        public object OtherFirstName { get; set; }
    }

    public partial class User
    {
        [JsonProperty("username")]
        public string Username { get; set; }

        [JsonProperty("email")]
        public string Email { get; set; }

        [JsonProperty("activeAccount")]
        public bool ActiveAccount { get; set; }

        [JsonProperty("password")]
        public object Password { get; set; }
    }

    public partial class EmployeeId
    {
        public static EmployeeId FromJson(string json) => JsonConvert.DeserializeObject<EmployeeId>(json, JsonConverterSettings.Settings);
    }

    public static class Serialize
    {
        public static string ToJson(this EmployeeId self) => JsonConvert.SerializeObject(self, JsonConverterSettings.Settings);
        public static string ToJsonIgnoreNull(this EmployeeId self) => JsonConvert.SerializeObject(self, JsonConverterSettings.NullSettings);
    }    
}
