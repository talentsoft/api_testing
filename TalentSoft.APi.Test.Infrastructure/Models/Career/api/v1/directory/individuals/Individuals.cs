﻿namespace TalentSoft.APi.Test.Infrastructure.Models.Career.api.v1.directory.individuals
{

    using System;
    using Newtonsoft.Json;
    using System.Collections.Generic;
    using TalentSoft.APi.Test.Infrastructure.Converters;

    public partial class Individuals
    {
        [JsonProperty("totalCount", NullValueHandling = NullValueHandling.Ignore)]
        public long? TotalCount { get; set; }

        [JsonProperty("results", NullValueHandling = NullValueHandling.Ignore)]
        public List<Result> Results { get; set; }
    }

    public partial class Result
    {
        [JsonProperty("individualId", NullValueHandling = NullValueHandling.Ignore)]
        public long? IndividualId { get; set; }

        [JsonProperty("firstName", NullValueHandling = NullValueHandling.Ignore)]
        public string FirstName { get; set; }

        [JsonProperty("lastName", NullValueHandling = NullValueHandling.Ignore)]
        public string LastName { get; set; }

        [JsonProperty("user", NullValueHandling = NullValueHandling.Ignore)]
        public User User { get; set; }

        [JsonProperty("sex", NullValueHandling = NullValueHandling.Ignore)]
        public long? Sex { get; set; }

        [JsonProperty("civilStatus", NullValueHandling = NullValueHandling.Ignore)]
        public CivilStatus CivilStatus { get; set; }

        [JsonProperty("birthdate", NullValueHandling = NullValueHandling.Ignore)]
        public DateTimeOffset? Birthdate { get; set; }

        [JsonProperty("isDefaultImage", NullValueHandling = NullValueHandling.Ignore)]
        public bool? IsDefaultImage { get; set; }

        [JsonProperty("pictureUrl", NullValueHandling = NullValueHandling.Ignore)]
        public string PictureUrl { get; set; }

        [JsonProperty("modificationDate", NullValueHandling = NullValueHandling.Ignore)]
        public DateTimeOffset? ModificationDate { get; set; }

        [JsonProperty("creationDate", NullValueHandling = NullValueHandling.Ignore)]
        public DateTimeOffset? CreationDate { get; set; }
    }

    public partial class CivilStatus
    {
        [JsonProperty("nationality")]
        public object Nationality { get; set; }

        [JsonProperty("title")]
        public object Title { get; set; }

        [JsonProperty("birthName")]
        public object BirthName { get; set; }

        [JsonProperty("otherFirstName")]
        public object OtherFirstName { get; set; }
    }

    public partial class User
    {
        [JsonProperty("username", NullValueHandling = NullValueHandling.Ignore)]
        public string Username { get; set; }

        [JsonProperty("email", NullValueHandling = NullValueHandling.Ignore)]
        public string Email { get; set; }

        [JsonProperty("activeAccount", NullValueHandling = NullValueHandling.Ignore)]
        public bool? ActiveAccount { get; set; }

        [JsonProperty("password")]
        public object Password { get; set; }
    }
    public partial class Individuals
    {
        public static Individuals FromJson(string json) => JsonConvert.DeserializeObject<Individuals>(json, JsonConverterSettings.Settings);
        public static Individuals FromJsonIgnoreNull(string json) => JsonConvert.DeserializeObject<Individuals>(json, JsonConverterSettings.NullSettings);
    }

    public static class Serialize
    {
        public static string ToJson(this Individuals self) => JsonConvert.SerializeObject(self, JsonConverterSettings.Settings);
        public static string ToJsonIgnoreNull(this Individuals self) => JsonConvert.SerializeObject(self, JsonConverterSettings.NullSettings);
    }

}
