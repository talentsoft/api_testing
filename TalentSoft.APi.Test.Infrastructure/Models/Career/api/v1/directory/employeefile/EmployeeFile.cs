﻿namespace TalentSoft.APi.Test.Infrastructure.Models.Career.api.v1.directory.employeefile
{
    using Newtonsoft.Json;
    using TalentSoft.APi.Test.Infrastructure.Converters;

    public partial class EmployeeFile
    {
        [JsonProperty("employeeNumber")]
        public string EmployeeNumber { get; set; }

        [JsonProperty("firstName")]
        public string FirstName { get; set; }

        [JsonProperty("lastName")]
        public string LastName { get; set; }

        [JsonProperty("photoUrl")]
        public string PhotoUrl { get; set; }

        [JsonProperty("gender")]
        public Gender Gender { get; set; }
    }

    public partial class Gender
    {
        [JsonProperty("code")]
        public string Code { get; set; }

        [JsonProperty("label")]
        public string Label { get; set; }
    }
    public partial class EmployeeFile
    {
        public static EmployeeFile FromJson(string json) => JsonConvert.DeserializeObject<EmployeeFile>(json, JsonConverterSettings.Settings);
        public static EmployeeFile FromJsonIgnoreNull(string json) => JsonConvert.DeserializeObject<EmployeeFile>(json, JsonConverterSettings.NullSettings);
    }
}
