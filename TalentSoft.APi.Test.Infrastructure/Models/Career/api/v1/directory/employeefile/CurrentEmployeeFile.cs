﻿namespace TalentSoft.APi.Test.Infrastructure.Models.Career.api.v1.directory.employeefile
{
    using System;
    using Newtonsoft.Json;
    using TalentSoft.APi.Test.Infrastructure.Converters;
    

    public partial class CurrentEmployeeFile
    {
        [JsonProperty("birthdate")]
        public Date Birthdate { get; set; }

        [JsonProperty("groupStartDate")]
        public Date GroupStartDate { get; set; }

        [JsonProperty("companyStartDate")]
        public Date CompanyStartDate { get; set; }

        [JsonProperty("managerId")]
        public string ManagerId { get; set; }

        [JsonProperty("legal")]
        public Gender Legal { get; set; }

        [JsonProperty("unit")]
        public Gender Unit { get; set; }

        [JsonProperty("job")]
        public Gender Job { get; set; }

        [JsonProperty("mail")]
        public string Mail { get; set; }

        [JsonProperty("payrollTitle")]
        public string PayrollTitle { get; set; }

        [JsonProperty("employeeNumber")]
        public string EmployeeNumber { get; set; }

        [JsonProperty("firstName")]
        public string FirstName { get; set; }

        [JsonProperty("lastName")]
        public string LastName { get; set; }

        [JsonProperty("photoUrl")]
        public string PhotoUrl { get; set; }

        [JsonProperty("gender")]
        public Gender Gender { get; set; }
    }

    public partial class Date
    {
        [JsonProperty("asDateTime")]
        public DateTimeOffset AsDateTime { get; set; }

        [JsonProperty("year")]
        public long Year { get; set; }

        [JsonProperty("month")]
        public long Month { get; set; }

        [JsonProperty("day")]
        public long Day { get; set; }
    }
    public partial class CurrentEmployeeFile
    {
        public static CurrentEmployeeFile FromJson(string json) => JsonConvert.DeserializeObject<CurrentEmployeeFile>(json, JsonConverterSettings.Settings);
        public static CurrentEmployeeFile FromJsonIgnoreNull(string json) => JsonConvert.DeserializeObject<CurrentEmployeeFile>(json, JsonConverterSettings.NullSettings);
    }

}
