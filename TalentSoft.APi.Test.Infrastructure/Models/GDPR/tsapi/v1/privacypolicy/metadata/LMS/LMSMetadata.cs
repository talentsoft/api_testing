﻿namespace TalentSoft.APi.Test.Infrastructure.Models.GDPR.tsapi.v1.privacypolicy.metadata.LMS
{
    using Newtonsoft.Json;
    using System.Collections.Generic;
    using TalentSoft.APi.Test.Infrastructure.Converters;

    public partial class LmsMetadata
    {
        [JsonProperty("supportedSearchFields")]
        public List<string> SupportedSearchFields { get; set; }

        [JsonProperty("supportedDataSubjectsTypes")]
        public List<SupportedDataSubjectsType> SupportedDataSubjectsTypes { get; set; }

        [JsonProperty("supportedDataSetsTypes")]
        public List<SupportedDataSetsType> SupportedDataSetsTypes { get; set; }
    }

    public partial class SupportedDataSetsType
    {
        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }

        [JsonProperty("contents")]
        public List<object> Contents { get; set; }
    }

    public partial class SupportedDataSubjectsType
    {
        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("dataSetTypes")]
        public List<string> DataSetTypes { get; set; }
    }
    public partial class LmsMetadata
    {
        public static LmsMetadata FromJson(string json) => JsonConvert.DeserializeObject<LmsMetadata>(json, JsonConverterSettings.Settings);
    }

    public static class Serialize
    {
        public static string ToJson(this LmsMetadata self) => JsonConvert.SerializeObject(self, JsonConverterSettings.Settings);
        public static string ToJsonIgnoreNull(this LmsMetadata self) => JsonConvert.SerializeObject(self, JsonConverterSettings.NullSettings);
    }
}
