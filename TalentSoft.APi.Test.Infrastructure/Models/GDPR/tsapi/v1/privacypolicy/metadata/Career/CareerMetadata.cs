﻿namespace TalentSoft.APi.Test.Infrastructure.Models.GDPR.tsapi.v1.privacypolicy.metadata.Career
{
    using Newtonsoft.Json;
    using System.Collections.Generic;
    using TalentSoft.APi.Test.Infrastructure.Converters;

    public partial class CareerMetadata
    {
        [JsonProperty("supportedSearchFields")]
        public List<string> SupportedSearchFields { get; set; }

        [JsonProperty("supportedDataSubjectTypes")]
        public List<SupportedDataSubjectType> SupportedDataSubjectTypes { get; set; }

        [JsonProperty("supportedDataSetTypes")]
        public List<SupportedDataSetType> SupportedDataSetTypes { get; set; }
    }

    public partial class SupportedDataSetType
    {
        [JsonProperty("type")]
        public string Type { get; set; }

        [JsonProperty("description")]
        public object Description { get; set; }

        [JsonProperty("contents")]
        public object Contents { get; set; }
    }

    public partial class SupportedDataSubjectType
    {
        [JsonProperty("type")]
        public string Type { get; set; }

        [JsonProperty("dataSetTypes")]
        public List<string> DataSetTypes { get; set; }
    }
    public partial class CareerMetadata
    {
        public static CareerMetadata FromJson(string json) => JsonConvert.DeserializeObject<CareerMetadata>(json, JsonConverterSettings.Settings);
    }

    public static class Serialize
    {
        public static string ToJson(this CareerMetadata self) => JsonConvert.SerializeObject(self, JsonConverterSettings.Settings);
        public static string ToJsonIgnoreNull(this CareerMetadata self) => JsonConvert.SerializeObject(self, JsonConverterSettings.NullSettings);
    }
}
