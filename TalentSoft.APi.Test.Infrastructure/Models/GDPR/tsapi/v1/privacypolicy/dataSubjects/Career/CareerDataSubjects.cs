﻿namespace TalentSoft.APi.Test.Infrastructure.Models.GDPR.tsapi.v1.privacypolicy.dataSubjects.Career
{
    using System;
    using Newtonsoft.Json;
    using System.Collections.Generic;
    using TalentSoft.APi.Test.Infrastructure.Converters;

    public partial class CareerDataSubjects
    {
        [JsonProperty("metadata")]
        public Metadata Metadata { get; set; }

        [JsonProperty("data")]
        public List<Datum> Data { get; set; }
    }

    public partial class Datum
    {
        [JsonProperty("uniqueIds")]
        public UniqueIds UniqueIds { get; set; }

        [JsonProperty("firstName")]
        public string FirstName { get; set; }

        [JsonProperty("lastName")]
        public string LastName { get; set; }

        [JsonProperty("birthdate")]
        public DateTimeOffset? Birthdate { get; set; }

        [JsonProperty("gender")]
        public string Gender { get; set; }

        [JsonProperty("endContractDate")]
        public DateTimeOffset? EndContractDate { get; set; }

        [JsonProperty("type")]
        public string Type { get; set; }

        [JsonProperty("email")]
        public List<string> Email { get; set; }

        [JsonProperty("phoneNumbers")]
        public List<string> PhoneNumbers { get; set; }

        [JsonProperty("addresses")]
        public List<string> Addresses { get; set; }
    }

    public partial class UniqueIds
    {
        [JsonProperty("userName")]
        public string UserName { get; set; }

        [JsonProperty("userKey")]
        public object UserKey { get; set; }

        [JsonProperty("employeeNumber")]
        public string EmployeeNumber { get; set; }

        [JsonProperty("careerIndividualId")]
        public long? CareerIndividualId { get; set; }

        [JsonProperty("careerTrainerId")]
        public object CareerTrainerId { get; set; }

        [JsonProperty("hubOnboardId")]
        public long? HubOnboardId { get; set; }

        [JsonProperty("hubRelativeId")]
        public long? HubRelativeId { get; set; }
    }

    public partial class Metadata
    {
        [JsonProperty("limit")]
        public long Limit { get; set; }

        [JsonProperty("offset")]
        public long Offset { get; set; }

        [JsonProperty("totalCount")]
        public long TotalCount { get; set; }
    }
    public partial class CareerDataSubjects
    {
        public static CareerDataSubjects FromJson(string json) => JsonConvert.DeserializeObject<CareerDataSubjects>(json, JsonConverterSettings.Settings);
    }

    public static class Serialize
    {
        public static string ToJson(this CareerDataSubjects self) => JsonConvert.SerializeObject(self, JsonConverterSettings.Settings);
        public static string ToJsonIgnoreNull(this CareerDataSubjects self) => JsonConvert.SerializeObject(self, JsonConverterSettings.NullSettings);
    }
}
