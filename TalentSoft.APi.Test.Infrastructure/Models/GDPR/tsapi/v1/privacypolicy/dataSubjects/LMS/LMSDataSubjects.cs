﻿namespace TalentSoft.APi.Test.Infrastructure.Models.GDPR.tsapi.v1.privacypolicy.dataSubjects.LMS
{
    using System;
    using Newtonsoft.Json;
    using System.Collections.Generic;
    using TalentSoft.APi.Test.Infrastructure.Converters;

    public partial class LmsDataSubjects
    {
        [JsonProperty("metadata")]
        public Metadata Metadata { get; set; }

        [JsonProperty("data")]
        public List<Datum> Data { get; set; }
    }

    public partial class Datum
    {
        [JsonProperty("uniqueIds")]
        public UniqueIds UniqueIds { get; set; }

        [JsonProperty("type")]
        public string Type { get; set; }

        [JsonProperty("firstName")]
        public string FirstName { get; set; }

        [JsonProperty("lastName")]
        public string LastName { get; set; }

        [JsonProperty("email")]
        public List<string> Email { get; set; }

        [JsonProperty("links")]
        public Links Links { get; set; }
    }

    public partial class Links
    {
        [JsonProperty("picture")]
        public Picture Picture { get; set; }
    }

    public partial class Picture
    {
        [JsonProperty("href")]
        public Uri Href { get; set; }
    }

    public partial class UniqueIds
    {
        [JsonProperty("userName")]
        public string UserName { get; set; }
    }

    public partial class Metadata
    {
        [JsonProperty("limit")]
        public long Limit { get; set; }

        [JsonProperty("offset")]
        public long Offset { get; set; }

        [JsonProperty("totalCount")]
        public long TotalCount { get; set; }
    }
    public partial class LmsDataSubjects
    {
        public static LmsDataSubjects FromJson(string json) => JsonConvert.DeserializeObject<LmsDataSubjects>(json, JsonConverterSettings.Settings);
    }

    public static class Serialize
    {
        public static string ToJson(this LmsDataSubjects self) => JsonConvert.SerializeObject(self, JsonConverterSettings.Settings);
        public static string ToJsonIgnoreNull(this LmsDataSubjects self) => JsonConvert.SerializeObject(self, JsonConverterSettings.NullSettings);
    }
}


