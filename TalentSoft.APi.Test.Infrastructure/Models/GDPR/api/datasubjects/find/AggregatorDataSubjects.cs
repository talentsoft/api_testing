﻿namespace TalentSoft.APi.Test.Infrastructure.Models.GDPR.api.datasubjects.find
{
    using System;
    using Newtonsoft.Json;
    using System.Collections.Generic;
    using TalentSoft.APi.Test.Infrastructure.Converters;

    public partial class AggregatorDataSubjects
    {
        [JsonProperty("uniqueIds")]
        public UniqueIds UniqueIds { get; set; }

        [JsonProperty("type")]
        public object Type { get; set; }

        [JsonProperty("types")]
        public List<string> Types { get; set; }

        [JsonProperty("dataSetTypes")]
        public List<long> DataSetTypes { get; set; }

        [JsonProperty("firstName")]
        public string FirstName { get; set; }

        [JsonProperty("lastName")]
        public string LastName { get; set; }

        [JsonProperty("birthdate")]
        public DateTimeOffset? Birthdate { get; set; }

        [JsonProperty("gender")]
        public string Gender { get; set; }

        [JsonProperty("endContractDate")]
        public DateTimeOffset? EndContractDate { get; set; }

        [JsonProperty("links")]
        public object Links { get; set; }

        [JsonProperty("email")]
        public List<string> Email { get; set; }

        [JsonProperty("phoneNumbers")]
        public List<string> PhoneNumbers { get; set; }

        [JsonProperty("addresses")]
        public List<string> Addresses { get; set; }
    }

    public partial class UniqueIds
    {
        [JsonProperty("userName")]
        public string UserName { get; set; }

        [JsonProperty("userKey")]
        public object UserKey { get; set; }

        [JsonProperty("employeeNumber")]
        public string EmployeeNumber { get; set; }

        [JsonProperty("careerIndividualId")]
        public long? CareerIndividualId { get; set; }

        [JsonProperty("careerTrainerId")]
        public object CareerTrainerId { get; set; }

        [JsonProperty("hubOnboardId")]
        public long? HubOnboardId { get; set; }

        [JsonProperty("hubRelativeId")]
        public long? HubRelativeId { get; set; }
    }
}
