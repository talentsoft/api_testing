﻿namespace TalentSoft.APi.Test.TestsRunner.Career
{
    using NUnit.Framework;
    using TalentSoft.APi.Test.Tests;
    using TalentSoft.APi.Test.Infrastructure.TestInitializer;

    [TestFixture]
    [Parallelizable(ParallelScope.Fixtures)]
    public class CareerTestRunner : TestInitializer
    {
        public CareerTestRunner() => Module = "career";

        [Test]
        public void CallingAPiWithLoginMethodPostResponseIsBadRequest()
        {
            Fixtures.EmployeeTests.Given_A_Api_With_Login_Method_POST_Response_is_BadRequest();
        }
        [Test]
        public void CallingAPiWithoutLoginResponseIsUnauthorized()
        {
            Fixtures.EmployeeTests.Given_A_Api_Without_Login_Response_is_Unauthorized();
        }
        [Test]
        public void CallingAPiWithLoginResponseIsOk()
        {
            Fixtures.EmployeeTests.Given_A_Api_With_Login_Response_is_OK();
        }
        [Test]
        public void CallingAPiWithLoginResponseContainsIndividualIdName()
        {
            Fixtures.EmployeeTests.Given_A_Api_With_Login_Response_Contains_IndividualId_Name();
        }
    }
}
