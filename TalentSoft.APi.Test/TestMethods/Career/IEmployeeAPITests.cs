﻿namespace TalentSoft.APi.Test.TestMethods.Career
{
    public interface IEmployeeApiTests
    {
        void Given_A_Api_Without_Login_Response_is_Unauthorized();
        void Given_A_Api_With_Login_Method_POST_Response_is_BadRequest();
        void Given_A_Api_With_Login_Response_is_OK();
    }
}
