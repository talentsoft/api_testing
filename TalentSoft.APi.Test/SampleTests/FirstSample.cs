namespace TalentSoft.APi.Test.SampleTests
{
    using RestSharp;
    using System.Net;
    using NUnit.Framework;
    using TalentSoft.APi.Test.Infrastructure.Client;
    using TalentSoft.APi.Test.Infrastructure.Helpers;
    using TalentSoft.APi.Test.Infrastructure.TestInitializer;

    [TestFixture]
    [Parallelizable(ParallelScope.Fixtures)]
    public class FirstSample : TestInitializer
    {
        private static RestClient _client;
        public FirstSample()
        {
            _client = new RestApiClient().CreateApp2AppTokenClient("career");
        }

        [Test]
        public void Given_a_Api_With_Authorization_Method_GET_Response_Is_OK()
        {
            var request = RequestHelper.CreateRequestWithMethod("tsapi/v1/privacypolicy/dataSubjects?filter=lastName eq 'as'", Method.GET);
            var response = _client.Execute(request);

            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
        }
    }
}