﻿namespace TalentSoft.APi.Test.Tests
{
    using TalentSoft.APi.Test.TestMethods.Career;
    using TalentSoft.APi.Test.Tests.Career.EmployeeAPI;

    public static class Fixtures
    {
        private static EmployeeTests _employeeTest;

        public static IEmployeeApiTests EmployeeTests =>
            _employeeTest ?? (_employeeTest = new EmployeeTests());
    }
}
