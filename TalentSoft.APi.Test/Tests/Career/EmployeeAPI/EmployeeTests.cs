﻿using TalentSoft.APi.Test.TestMethods.Career;

namespace TalentSoft.APi.Test.Tests.Career.EmployeeAPI
{
    using RestSharp;
    using System.Net;
    using System.Linq;
    using NUnit.Framework;
    using FluentAssertions;
    using TalentSoft.APi.Test.Infrastructure.Managers;
    using TalentSoft.APi.Test.Infrastructure.Helpers;
    using TalentSoft.APi.Test.Infrastructure.Client;

    public class EmployeeTests : IEmployeeApiTests
    {
        private static RestClient _client;
        public EmployeeTests()
        {
            _client = new RestApiClient().CreateCookieClient("career");
        }
        public void Given_A_Api_Without_Login_Response_is_Unauthorized()
        {
            var clientWithoutCooke = new RestClient(ApiManager.ApiEndpointBase("career"));

            using (var sqlManager = new SqlServerAccessManager("career"))
            {
                sqlManager.Connect();
                var script = @"select top 1 EmployeeNumber from Individual.Employees
                               Order By NEWID()";
                var data = sqlManager.ExecuteSql(script).ToList();
                var employeeId = data.Single().EmployeeNumber.ToString();
                var request = RequestHelper.CreateRequest("api/v1/directory/employees/{employeeId}");
                request.AddUrlSegment("employeeId", $"{employeeId}");
                var response = clientWithoutCooke.Execute(request);

                Assert.AreEqual(HttpStatusCode.Unauthorized, response.StatusCode);
                response.StatusCode.Should().Be(HttpStatusCode.Unauthorized);
            }
        }

        public void Given_A_Api_With_Login_Method_POST_Response_is_BadRequest()
        {
            using (var sqlManager = new SqlServerAccessManager("career"))
            {
                sqlManager.Connect();
                var script = @"select top 1 EmployeeNumber from Individual.Employees
                               Order By NEWID()";
                var data = sqlManager.ExecuteSql(script).ToList();
                var employeeId = data.Single().EmployeeNumber.ToString();
                var request = RequestHelper.CreateRequestWithMethod("api/v1/directory/employees/{employeeId}", Method.POST);
                request.AddUrlSegment("employeeId", $"{employeeId}");
                var response = _client.Execute(request);

                Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
            }
        }

        public void Given_A_Api_With_Login_Response_is_OK()
        {
            using (var sqlManager = new SqlServerAccessManager("career"))
            {
                sqlManager.Connect();
                var script = @"select top 1 EmployeeNumber from Individual.Employees
                               Order By NEWID()";
                var data = sqlManager.ExecuteSql(script).ToList();
                var employeeId = data.Single().EmployeeNumber.ToString();
                var request = RequestHelper.CreateRequest("api/v1/directory/employees/{employeeId}");
                request.AddUrlSegment("employeeId", $"{employeeId}");
                var response = _client.Execute(request);

                Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
            }
        }
    }
}
